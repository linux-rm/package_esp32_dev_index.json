# 加速Arduino-ESP-32的配置

# 配置方法:

## 1. 下载安装并打开[Arduino IDE](https://www.arduino.cc/en/software)
![Arduino](README_files/1.png)

## 2. 打开"首选项"
![首选项](README_files/2.png)

## 3. 修改附加开发板的地址
`https://gitee.com/linux-rm/package_esp32_dev_index.json/raw/master/package_esp32_dev_index.json`
![开发板的地址](README_files/3.png)

## 4. 关闭Arduino

## 5. 打开文件管理器或终端,进入Arduino开发板路径
	* 不同系统的路径有差异
	* windows:`%HOMEPATH%\AppData\Local\Arduino15\packages`
	* Linux:`~/.arduino15`
	* MacOS:`~/Library/Arduino15`
![目录](README_files/5.png)

## 6. 删除`cache`和`package_esp32_dev_index.json`(如果有)
![删除](README_files/6.png)

## 7. 再次打开Arduino IDE,打开Arduino开发板管理器
![开发板管理器](README_files/7.png)

## 8. 搜索 __ESP32__ 选择最新版本下载
如果提示失败,请提交[issus](https://gitee.com/linux-rm/package_esp32_dev_index.json/issues/new)
![搜索](README_files/8.png)

## 9. 选择开发板以及端口和开发板选项(例如Flash大小)
![开发板](README_files/9.png)

## 10. 编写代码,编译,上传,测试
示例代码,正常运行可以在串口监视器(速率115200)看到Hello World
```cpp
void setup(){Serial.begin(115200);}
void loop(){Serial.println("Hello World");delay(1000);}
```